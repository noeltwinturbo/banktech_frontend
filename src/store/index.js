import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: null,
    cookie: null,
    isLoading: false,
    isChargin: true,
    alert: {
      isEnabled: false,
      msg: "",
      type: null
    },
    generalKey: 0,
  },
  mutations: {
    setUser(state, newUser) {
      state.user = newUser
    },
    enableLoading(state) {
      state.isLoading = true
    },
    disableLoading(state) {
      state.isLoading = false
    },
    enableError(state, msg) {
      state.alert.isEnabled = true
      state.alert.msg = msg
      state.alert.color = "error"
      state.alert.icon = "mdi-alert-circle"
    },
    enableSuccess(state, msg) {
      state.alert.isEnabled = true
      state.alert.msg = msg
      state.alert.color = "success"
      state.alert.icon = "mdi-checkbox-marked-circle-outline"
    },
    disableAlert(state) {
      state.alert.isEnabled = false
    },
    updateGeneralKey(state) {
      state.generalKey++;
    },
    disableChargin(state) {
      state.isChargin = false
    },
    getCookie(state, cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(";");
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == " ") {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          var cookie = c.substring(name.length, c.length);
          state.cookie = JSON.parse(atob(cookie.split(".")[1]));
          break;
        }
      }
    }
  },
  actions: {
    getUser: async function ({ commit }) {
      if (this.state.cookie != null) {
        commit("enableLoading");
        await axios
          .get("/users", this.login)
          .then(response => {
            commit("setUser", response.data);
            commit("disableChargin")
          })
          .catch(error => {
            commit("enableError", error.response.data.message);
          })
          .finally(
            () => { if (!window.location.pathname.includes("account") && !window.location.pathname.includes("movement")) { commit("disableLoading") } }
          );
      } else {
        commit("disableChargin");
        if (window.location.pathname != "/home") {
          commit("enableError", "No te has atenticado todavía!");
        }
      }
    },
  }
})
