import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'home',
    component: () => import('../views/Home.vue')
  },
  { path: '/', redirect: '/home' },
  {
    path: '/personalData',
    name: 'personalData',
    component: () => import('../views/PersonalData.vue')
  },
  {
    path: '/accessData',
    name: 'accessData',
    component: () => import('../views/AccessData.vue')
  },
  {
    path: '/userConfigs',
    name: 'userConfigs',
    component: () => import('../views/UserConfigs.vue')
  },
  {
    path: '/accounts',
    name: 'accounts',
    component: () => import('../views/Accounts.vue')
  },
  {
    path: '/accounts/:accountId/movements',
    name: 'movements',
    component: () => import('../views/Movements.vue')
  },
  {
    path: '/envConfigs',
    name: 'envConfigs',
    component: () => import('../views/EnvConfigs.vue')
  },
  {
    path: '/logs',
    name: 'logs',
    component: () => import('../views/Logs.vue')
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/About.vue')
  },
  {
    path: '*',
    name: 'notFound',
    component: () => import('../views/NotFound.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router