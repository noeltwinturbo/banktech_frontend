export { UserDataRules, UserAccessDataRules, AccountRules, MovementRules, EnvConfigRules }

const PHONE_REGEX = /^\d{9}$/
const ZIPCODE_REGEX = /^\d{5}$/
const DNI_REGEX = /^\d{8}[A-Z]$/
const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
const PASSWORD_REGEX_1 = /[a-z]+/
const PASSWORD_REGEX_2 = /[A-Z]+/
const PASSWORD_REGEX_3 = /[0-9]+/
const PASSWORD_REGEX_4 = /[,;.:-_!@#$%^&*]+/
const PASSWORD_REGEX_5 = /\s+/
const PASSWORD_REGEX_6 = /^.{8,16}$/
const IBAN_REGEX = /^[A-Z]{2}\d{22}$/
const AMOUNT_REGEX = /^\d+(?:\.\d{1,2})?$/

const UserDataRules = {
    name: [
        value => !!value || 'Requerido',
        value => (value || '').length >= 3 || 'Mínimo 3 caracteres',
        value => (value || '').length <= 25 || 'Máxmimo 25 caracteres'
    ],
    lastName: [
        value => !!value || 'Requerido',
        value => (value || '').length >= 3 || 'Mínimo 3 caracteres',
        value => (value || '').length <= 50 || 'Máxmimo 50 caracteres'
    ],
    birthday: [
        value => !!value || 'Requerido'
    ],
    phone: [
        value => !!value || 'Requerido',
        value => PHONE_REGEX.test(value) || 'Número de teléfono no válido'
    ],
    street: [
        value => !!value || 'Requerido',
        value => (value || '').length >= 3 || 'Mínimo 3 caracteres',
        value => (value || '').length <= 75 || 'Máxmimo 75 caracteres'
    ],
    city: [
        value => !!value || 'Requerido',
        value => (value || '').length >= 3 || 'Mínimo 3 caracteres',
        value => (value || '').length <= 50 || 'Máxmimo 50 caracteres'
    ],
    country: [
        value => !!value || 'Requerido',
        value => (value || '').length >= 3 || 'Mínimo 3 caracteres',
        value => (value || '').length <= 50 || 'Máxmimo 50 caracteres'
    ],
    zipcode: [
        value => !!value || 'Requerido',
        value => ZIPCODE_REGEX.test(value) || 'Código postal no válido'
    ]
}

const UserAccessDataRules = {
    dni: [
        value => !!value || 'Requerido',
        value => DNI_REGEX.test(value) || 'DNI no válido'
    ],
    email: [
        value => !!value || 'Requerido',
        value => EMAIL_REGEX.test(value) || 'Email no válido'
    ],
    password: [
        value => !!value || 'Requerido',
        value => (PASSWORD_REGEX_1.test(value) && PASSWORD_REGEX_2.test(value) && PASSWORD_REGEX_3.test(value) && PASSWORD_REGEX_4.test(value) && !PASSWORD_REGEX_5.test(value) && PASSWORD_REGEX_6.test(value)) || 'La contraseña no es fuerte'
    ],
}

const AccountRules = {
    alias: [
        value => !!value || 'Requerido',
        value => (value || '').length >= 3 || 'Mínimo 3 caracteres',
        value => (value || '').length <= 50 || 'Máxmimo 50 caracteres'
    ],
    iban: [
        value => !!value || 'Requerido',
        value => IBAN_REGEX.test(value) || 'IBAN no válido'
    ]
}

const MovementRules = {
    concept: [
        value => !!value || 'Requerido',
        value => (value || '').length >= 3 || 'Mínimo 3 caracteres',
        value => (value || '').length <= 50 || 'Máxmimo 50 caracteres'
    ],
    amount: [
        value => !!value || 'Requerido',
        value => AMOUNT_REGEX.test(value) || 'Cantidad incorrecta'
    ]
}

const EnvConfigRules = {
    configName: [
        value => !!value || 'Requerido',
        value => (value || '').length >= 3 || 'Mínimo 3 caracteres',
        value => (value || '').length <= 100 || 'Máxmimo 100 caracteres'
    ],
    configValue: [
        value => !!value || 'Requerido',
        value => (value || '').length <= 100 || 'Máxmimo 100 caracteres'
    ]
}