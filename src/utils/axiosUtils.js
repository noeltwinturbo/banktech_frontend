import axios from "axios";
import { enabled } from "ansi-colors";

export { AxiosUser, AxiosUserData, AxiosUserAccessData, AxiosUserConfigs, AxiosAccounts, AxiosMovements, AxiosEnvConfigs, AxiosLogs }

const ENABLE_LOADING = "enableLoading"
const DISABLE_LOADING = "disableLoading"
const ENABLE_SUCCESS = "enableSuccess"
const ENABLE_ERROR = "enableError"

const AxiosUser = {
    postLoing: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .post("/users/login", vue.login)
            .then(response => {
                vue.$store.commit("setUser", response.data);
                vue.$store.commit("getCookie", "user_token");
                vue.$store.commit("updateGeneralKey");
                vue.showLogin = false;
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    },
    postLogout: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .post("/users/logout", {})
            .then(response => {
                document.cookie =
                    "user_token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                vue.$store.commit("setUser", null);
                vue.showLogout = false;
                vue.$router.push({ name: "home" });
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    },
    postResetPassword: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .post("/users/resetPassword", vue.login)
            .then(response => {
                vue.$store.commit(ENABLE_SUCCESS, response.data.message);
                vue.showLogin = false;
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    },
    post: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .post("/users", vue.userRegistry)
            .then(response => {
                vue.$store.commit("setUser", response.data);
                vue.showRegistry = false;
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    }
}

const AxiosUserData = {
    put: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .put("/users/data", vue.user)
            .then(response => {
                vue.$store.commit(ENABLE_SUCCESS, response.data.message);
                vue.isReadOnly = true;
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    }
}

const AxiosUserAccessData = {
    putDni: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .put("/users/dni", {
                dni: vue.dniAux
            })
            .then(response => {
                vue.$store.commit(ENABLE_SUCCESS, response.data.message);
                vue.user.dni = vue.dniAux;
                vue.showDni = false;
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    },
    putEmail: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .put("/users/email", {
                email: vue.emailAux
            })
            .then(response => {
                vue.$store.commit(ENABLE_SUCCESS, response.data.message);
                vue.user.email = vue.emailAux;
                vue.showEmail = false;
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    },
    putPassword: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .put("/users/password", {
                password: vue.newPassword1
            })
            .then(response => {
                vue.$store.commit(ENABLE_SUCCESS, response.data.message);
                vue.user.password = vue.newPassword1;
                vue.oldPassword = null;
                vue.newPassword1 = null;
                vue.newPassword2 = null;
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    },
    deleteUser: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .delete("/users")
            .then(response => {
                vue.$store.commit(ENABLE_SUCCESS, response.data.message);
                document.cookie =
                    "user_token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                vue.$store.commit("setUser", null);
                vue.$router.push({ name: "home" });
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    }
}

const AxiosAccounts = {
    getAll: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .get("/accounts")
            .then(response => {
                vue.accounts = response.data;
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    },
    get: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .get("/accounts/" + vue.$route.params.accountId)
            .then(response => {
                vue.account = response.data;
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    },
    post: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        vue.accountAux.owner = vue.user.dni;
        await axios
            .post("/accounts", vue.accountAux)
            .then(response => {
                vue.$store.commit(ENABLE_SUCCESS, "Cuenta bancaria añadida");
                vue.accounts.push(response.data);
                vue.localKey++;
                vue.showAdd = false;
            })
            .catch(error => {
                vue.$store.commit("enableError", error.response.data.message);
            })
            .finally(() => vue.$store.commit("disableLoading"));
    },
    putAlias: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .put("/accounts/" + vue.accountAux.accountId, {
                alias: vue.accountAux.alias
            })
            .then(response => {
                vue.$store.commit(ENABLE_SUCCESS, response.data.message);
                vue.accounts[
                    vue.accounts.findIndex(i => i.accountId == vue.accountAux.accountId)
                ] = JSON.parse(JSON.stringify(vue.accountAux));
                vue.localKey++;
                vue.showAliasChange = false;
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    },
    putOwner: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .put("/accounts/" + vue.accountAux.accountId + "/changeOwner", {
                dni: vue.accountAux.owner
            })
            .then(response => {
                vue.$store.commit(ENABLE_SUCCESS, response.data.message);
                vue.accounts[
                    vue.accounts.findIndex(i => i.accountId == vue.accountAux.accountId)
                ] = JSON.parse(JSON.stringify(vue.accountAux));
                vue.getAccounts();
                vue.localKey++;
                vue.showOwnerChange = false;
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    },
    delete: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .delete("/accounts/" + vue.accountIdTmp)
            .then(response => {
                vue.$store.commit(ENABLE_SUCCESS, response.data.message);
                vue.accounts.splice(
                    vue.accounts.findIndex(i => i.accountId == vue.accountIdTmp),
                    1
                );
                vue.showDelete = false;
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    }
}

const AxiosMovements = {
    get: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .get("/accounts/" + vue.$route.params.accountId + "/movements")
            .then(response => {
                vue.movements = response.data;
                vue.movements = vue.movements.sort(function (a, b) {
                    if (a.date > b.date) {
                        return -1;
                    }
                    if (a.date < b.date) {
                        return 1;
                    }
                    return 0;
                });
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    },
    post: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .post(
                "/accounts/" + vue.$route.params.accountId + "/movements",
                vue.movement
            )
            .then(response => {
                vue.$store.commit(
                    ENABLE_SUCCESS,
                    "Movimiento realizado correctamente"
                );
                vue.movements.push(response.data);
                vue.movements = vue.movements.sort(function (a, b) {
                    if (a.date > b.date) {
                        return -1;
                    }
                    if (a.date < b.date) {
                        return 1;
                    }
                    return 0;
                });
                vue.localKey++;
                vue.showAdd = false;
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    },
    delete: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .delete(
                "/accounts/" +
                vue.$route.params.accountId +
                "/movements/" +
                vue.movement.movementId
            )
            .then(response => {
                vue.$store.commit(ENABLE_SUCCESS, response.data.message);
                var mov = vue.movements[
                    vue.movements.findIndex(
                        i => i.movementId == vue.movement.movementId
                    )
                ];
                mov.isCanceled = true;
                vue.movements[
                    vue.movements.findIndex(
                        i => i.movementId == vue.movement.movementId
                    )
                ] = mov;
                vue.localKey++;
                vue.showRemove = false;
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    }
}

const AxiosUserConfigs = {
    put: async function (vue) {
        vue.user.userConfig.minMovAmntNotify = parseInt(
            vue.user.userConfig.minMovAmntNotify
        );
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .put("/users/config", vue.user.userConfig)
            .then(response => {
                vue.$store.commit(ENABLE_SUCCESS, response.data.message);
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    }
}

const AxiosEnvConfigs = {
    get: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .get("/envConfigs")
            .then(response => {
                vue.envConfigs = response.data;
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    },
    post: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .post("/envConfigs", vue.envConfigAux)
            .then(response => {
                vue.$store.commit(ENABLE_SUCCESS, response.data.message);
                vue.envConfigs.push(vue.envConfigAux);
                vue.localKey++;
                vue.showAdd = false;
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    },
    put: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .put("/envConfigs", vue.envConfigAux)
            .then(response => {
                vue.$store.commit(ENABLE_SUCCESS, response.data.message);
                vue.envConfigs[
                    vue.envConfigs.findIndex(
                        i => i.configName == vue.envConfigAux.configName
                    )
                ] = vue.envConfigAux;
                vue.localKey++;
                vue.showEdit = false;
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    },
    delete: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .delete("/envConfigs/" + vue.envConfigAux.configName)
            .then(response => {
                vue.$store.commit("enableSuccess", response.data.message);
                vue.envConfigs.splice(
                    vue.envConfigs.findIndex(i => i.configName == vue.envConfigAux.configName),
                    1
                );
                vue.showDelete = false;
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    },
    reload: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .post("/envConfigs/reload")
            .then(response => {
                vue.$store.commit(ENABLE_SUCCESS, response.data.message);
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    }
}

const AxiosLogs = {
    get: async function (vue) {
        vue.$store.commit(ENABLE_LOADING);
        await axios
            .get("/envConfigs/logs")
            .then(response => {
                vue.logs = response.data
            })
            .catch(error => {
                vue.$store.commit(ENABLE_ERROR, error.response.data.message);
            })
            .finally(() => vue.$store.commit(DISABLE_LOADING));
    }
}